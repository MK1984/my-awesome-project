package carrental.carrental.model;


import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "rent")
@SequenceGenerator(name = "rentSeq", sequenceName = "rent_id_seq", allocationSize = 1)
public class Rent {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rentSeq")
    private Integer id;
    @Column(name = "rent_date")
    private Timestamp rentDate;
    private String comments;
    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employee employee;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getRentDate() {
        return rentDate;
    }

    public void setRentDate(Timestamp rentDate) {
        this.rentDate = rentDate;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
