package carrental.carrental.domain.reservation;

import carrental.carrental.model.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReservationRepository extends JpaRepository<Reservation , Integer> {
}
