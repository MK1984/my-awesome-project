package carrental.carrental.model;

public enum Status {
    RENTED,
    AVAILABLE,
    INACCESSIBLE
}
