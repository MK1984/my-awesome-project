package carrental.carrental.model;


import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Table(name = "return")
@SequenceGenerator(name = "returnSeq", sequenceName = "return_id_seq", allocationSize = 1)
public class Return {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "returnSeq")
    private Integer id;
    @Column(name = "return_date")
    private Timestamp returnDate;
    private String comments;
    private BigDecimal surcharge;
    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employee employee;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public BigDecimal getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(BigDecimal surcharge) {
        this.surcharge = surcharge;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Timestamp getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Timestamp returnDate) {
        this.returnDate = returnDate;
    }
}
