package carrental.carrental.model;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "rental")
@SequenceGenerator(name = "rentalSeq", sequenceName = "rental_id_seq", allocationSize = 1)
public class Rental {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rentalSeq")
    private Integer id;
    @Column(name = "internet_domain")
    private String internetDomain;
    private String owner;
    private String logo;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInternetDomain() {
        return internetDomain;
    }

    public void setInternetDomain(String internetDomain) {
        this.internetDomain = internetDomain;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
