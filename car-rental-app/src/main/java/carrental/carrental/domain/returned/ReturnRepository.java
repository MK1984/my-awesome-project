package carrental.carrental.domain.returned;

import carrental.carrental.model.Return;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReturnRepository extends JpaRepository<Return, Integer> {
}
