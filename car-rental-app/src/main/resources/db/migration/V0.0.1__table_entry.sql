 create table rental
 (
     id serial not null
         constraint rental_pk
         primary key,
     internet_domain varchar not null,
     contact_address varchar not null,
     owner varchar not null,
     logo varchar not null


 );

create table employee
(
    id serial not null
        constraint employee_pk
        primary key,
    first_name varchar not null,
    last_name varchar not null,
    position varchar not null,
    id_department int not null
);


create table department
(
    id     serial  not null
        constraint department_pk
        primary key,
    street varchar not null,
    city   varchar not null,
    id_rental serial not null

);

alter table department
    owner to admin;

create table car
(
    id                           serial    not null
        constraint car_pk
            primary key,
    mark                         varchar   not null,
    model                        varchar   not null,
    body_type                    varchar   not null,
    production_date              timestamp not null,
    color                        varchar   not null,
    car_mileage                  integer   not null,
    status                       varchar   not null,
    amount_for_the_day_of_rental numeric     not null,
        id_department int not null

);

alter table car
    owner to admin;

create table customer
(
    id         serial  not null
        constraint customer_pk
        primary key,
    first_name varchar not null,
    last_name  varchar not null,
    email      varchar not null,
    address    varchar not null
);

alter table customer
    owner to admin;

create table reservation
(
    id serial not null
        constraint reservation_pk
        primary key,
    reservation_date time not null,
    id_customer int not null,
    id_car int not null,
    from_date timestamp not null,
    to_date timestamp,
    id_rental_department int not null,
    id_return_department int,
    summary_rental_price numeric
);

create table rent
(
    id serial not null
        constraint rent_pk
        primary key,
    id_employee int not null,
    rent_date timestamp not null,
    comments varchar

);

create table return
(
    id serial not null
        constraint return_pk
        primary key,
    id_employee int not null,
    return_date timestamp not null,
    surcharge numeric,
    comments varchar
);







-- foreign keys
 alter table department
     add constraint department_rental_id_fk
         foreign key (id_rental) references rental (id)
             on update cascade on delete restrict;

alter table employee
    add constraint employee_department_id_fk
        foreign key (id_department) references department
            on update cascade on delete restrict;

alter table car
    add constraint car_department_id_fk
        foreign key (id_department) references department
            on update cascade on delete restrict;


 alter table rent
     add constraint rent_employee_id_fk
         foreign key (id_employee) references rent
             on update cascade on delete restrict;

alter table return
    add constraint return_employee_id_fk
        foreign key (id_employee) references return
            on update cascade on delete restrict;

alter table reservation
    add constraint reservation_car_id_fk
        foreign key (id_car) references car
            on update cascade on delete restrict;

alter table reservation
    add constraint reservation_customer_id_fk
        foreign key (id_customer) references customer
            on update cascade on delete restrict;

alter table reservation
    add constraint reservation_department_id_fk
        foreign key (id_return_department) references department
            on update cascade on delete restrict;

alter table reservation
    add constraint reservation_department_id_fk_2
        foreign key (id_rental_department) references department
            on update cascade on delete restrict;



