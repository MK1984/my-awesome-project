package carrental.carrental.model;


import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Table(name = "car")
@SequenceGenerator(name = "carSeq", sequenceName = "car_id_seq", allocationSize = 1)
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "carSeq")
    private Integer id;
    private String mark;
    private String model;
    @Column(name = "body_type")
    private String bodyType;
    @Column(name = "production_date")
    private Timestamp productionDate;
    private String color;
    @Column(name = "car_mileage")
    private Integer carMileage;
    @Enumerated(EnumType.STRING)
    private Status status;
    @Column(name = "amount_for_the_day_of_rental")
    private BigDecimal amountPerDay;
    @ManyToOne
    @JoinColumn(name = "id_department")
    private Department department;

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public Timestamp getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(Timestamp productionDate) {
        this.productionDate = productionDate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getCarMileage() {
        return carMileage;
    }

    public void setCarMileage(Integer carMileage) {
        this.carMileage = carMileage;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public BigDecimal getAmountPerDay() {
        return amountPerDay;
    }

    public void setAmountPerDay(BigDecimal amountPerDay) {
        this.amountPerDay = amountPerDay;
    }
}
