package carrental.carrental.domain.department;

import org.springframework.stereotype.Service;

@Service
public class DepartmentService{
    private final DepartmentRepository departmentRepository;

    public DepartmentService(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }
}
