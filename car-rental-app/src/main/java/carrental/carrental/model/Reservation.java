package carrental.carrental.model;


import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "reservation")
@SequenceGenerator(name = "reservationSeq", sequenceName = "reservation_id_seq", allocationSize = 1)
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reservationSeq")
    private Integer id;
    @Column(name = "reservation_date")
    private Timestamp reservationDate;
    @Column(name = "from_date")
    private Timestamp fromDate;
    @Column(name = "to_date")
    private Timestamp toDate;
    @Column(name = "summary_rental_price")
    private BigDecimal summaryRentalPrice;
    @ManyToOne
    @JoinColumn(name = "id_customer")
    private Customer customer;
    @ManyToOne
    @JoinColumn(name = "id_car")
    private Car car;
    @ManyToOne
    @JoinColumn(name = "id_rental_department")
    private Department rentalDepartment;
    @ManyToOne
    @JoinColumn(name = "id_return_department")
    private Department returnDepartment;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(Timestamp reservationDate) {
        this.reservationDate = reservationDate;
    }

    public Timestamp getFromDate() {
        return fromDate;
    }

    public void setFromDate(Timestamp fromDate) {
        this.fromDate = fromDate;
    }

    public Timestamp getToDate() {
        return toDate;
    }

    public void setToDate(Timestamp toDate) {
        this.toDate = toDate;
    }

    public BigDecimal getSummaryRentalPrice() {
        return summaryRentalPrice;
    }

    public void setSummaryRentalPrice(BigDecimal summaryRentalPrice) {
        this.summaryRentalPrice = summaryRentalPrice;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Department getRentalDepartment() {
        return rentalDepartment;
    }

    public void setRentalDepartment(Department rentalDepartment) {
        this.rentalDepartment = rentalDepartment;
    }

    public Department getReturnDepartment() {
        return returnDepartment;
    }

    public void setReturnDepartment(Department returnDepartment) {
        this.returnDepartment = returnDepartment;
    }
}
