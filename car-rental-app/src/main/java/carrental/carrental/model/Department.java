package carrental.carrental.model;

import javax.persistence.*;

@Entity
@Table(name = "department")
@SequenceGenerator(name = "departmentSeq", sequenceName = "department_id_seq", allocationSize = 1)
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "departmentSeq")
    private Integer id;
    private String street;
    private String city;
    @ManyToOne
    @JoinColumn(name = "id_rental")
    private Rental rental;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


}



